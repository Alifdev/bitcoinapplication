class WalletsController < ApplicationController
  def index
    @wallets = current_user.wallets
  end

  def create
    @wallet = GenerateBitcoinService.create_wallet!(current_user)
    redirect_to Wallet, notice: 'Wallet successfully created'
  end

  def show
    @wallet = current_user.wallets.find(params[:id])
  end

  def edit
    @wallet = current_user.wallets.find(params[:id])
  end

  def destroy
    @wallet = current_user.wallets.find(params[:id])
    @wallet.destroy!
    redirect_to Wallet, notice: 'Wallet successfully removed'
  end
end
