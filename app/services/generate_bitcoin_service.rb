class GenerateBitcoinService
  attr_reader :user

  def initialize(user)
    @user = user
  end

  def self.create_wallet!(user)
    new(user).create_wallet!
  end

  def create_wallet!
    Wallet.create!(
      address: bitcoin_address,
      key: wallet_key,
      user: user
    )
  end

  private def bitcoin_address
    address, _redeem_script = Bitcoin.pubkeys_to_p2sh_multisig_address(
      2,
      user_key,
      wallet_key,
      service_key
    )
    address
  end

  private def user_key
    @user_key ||= Bitcoin.generate_key.last
  end

  private def wallet_key
    @wallet_key ||= Bitcoin.generate_key.last
  end

  private def service_key
    Settings.private_keys.service
  end
end
