class CreateWallets < ActiveRecord::Migration[5.1]
  def change
    create_table :wallets do |t|
      t.string :address
      t.string :key
      t.references :user, index: true

      t.timestamps
    end
  end
end
