require 'rails_helper'

RSpec.describe GenerateBitcoinService do
  describe '.create_wallet!' do
    let(:user) { create(:user) }

    it 'creates wallet' do
      expect { described_class.create_wallet!(user) }
        .to change(Wallet, :count).from(0).to(1)
    end
  end
end
