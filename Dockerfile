FROM ruby:2.4.1

RUN apt-get update -qq && apt-get install -y build-essential nodejs libgmp3-dev libxml2-dev libxslt1-dev cron imagemagick

RUN mkdir /bitcoin
RUN curl -sL https://deb.nodesource.com/setup_6.x | bash && apt-get install -y nodejs

WORKDIR /bitcoin
ADD Gemfile /bitcoin/Gemfile
ADD Gemfile.lock /bitcoin/Gemfile.lock
RUN bundle install
ADD . /bitcoin
